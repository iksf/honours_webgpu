#version 450
layout(location=0) in vec4 position;
layout(location=1) in vec4 normals;
layout(location=2) in vec4 tangent;
layout(location=3) in vec2 tex_coord;
layout(location = 4) in vec4 model_matrix_x;
layout(location = 5) in vec4 model_matrix_y;
layout(location = 6) in vec4 model_matrix_z;
layout(location = 7) in vec4 model_matrix_w;
layout (location =0) out vec4 v_pos;
layout(location=1) out vec4 v_normals;
layout(location=2) out vec4 v_tangent;
layout(location=3) out vec2 v_tex_coord;


layout(set=1, binding=0)
uniform Uniforms  {
    mat4 u_view_proj;
};




void main() {

    mat4 model_matrix = mat4( model_matrix_x, model_matrix_y, model_matrix_z, model_matrix_w);
    v_pos =vec4(position.x, position.y, position.z, 1.0);
    v_normals =normals;
    v_tangent = tangent;
    v_tex_coord = tex_coord;


    gl_Position = u_view_proj    * model_matrix  * position;// 3.

}