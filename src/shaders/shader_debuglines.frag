#version 450
layout (location =0) in vec4 v_pos;
layout(location=1) in vec4 v_normals;
layout(location=2) in vec4 v_tangent;
layout(location=3) in vec2 v_tex_coord;


layout (location = 0) out vec4 f_colour;
layout (set = 0, binding = 0) uniform texture2D t_diffuse;
layout (set = 0, binding = 1) uniform sampler s_diffuse;
layout (set = 0, binding = 2) uniform texture2D t_metalness;
layout (set = 0, binding  = 3) uniform sampler s_metalness;
layout (set = 0, binding = 4) uniform texture2D t_roughness;
layout (set = 0, binding  = 5) uniform sampler s_roughness;



layout(set=2, binding=0)
uniform  Material {
    vec3 colour;

    float metalness;
    float roughness;
};


void main() {
  //  f_colour = texture(sampler2D (t_diffuse, s_diffuse), v_tex_coord);
   // f_colour = vec4(1.0, 0.0, 0.0,1.0);
    f_colour = vec4(1.0,1.0,1.0,1.0);
}