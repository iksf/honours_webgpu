use crate::Vertex;
use genmesh::generators::{IndexedPolygon, Plane, SharedVertex};
use genmesh::{
    EmitTriangles, Indexer, LruIndexer, MapToVertices, MapVertex, Quad, Triangulate, Vertices,
};

pub fn terrain(x: u32, y: u32) -> (Vec<Vertex>, Vec<i32>) {
    let mut vertices = vec![];
    let mut indexer = LruIndexer::new((x * y * 4) as usize, |a, v: genmesh::Vertex| {
        let mut perlin = noise::Perlin::new();
        use noise::{NoiseFn, Perlin};
        let add = perlin.get([v.pos.x as f64, v.pos.y as f64, v.pos.z as f64, 1.0]) * 0.00;
        let vertex_scale = 100.0;

        vertices.push(Vertex {
            position: [
                v.pos.x * vertex_scale,
                (v.pos.z) * vertex_scale + add as f32,
                v.pos.y * vertex_scale,
                1.0,
            ],
            tex_coords: [v.pos.x, v.pos.y],

            ..Default::default()
        })
    });
    let indices = Plane::subdivide(x as usize, y as usize)
        .triangulate()
        .vertex(|v| indexer.index(v))
        .vertices()
        .map(|x| x as i32)
        .collect();

    (vertices, indices)
}
