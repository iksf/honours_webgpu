mod math_types;
use std::default::Default;
mod particle_effect;
mod terrain;
mod texture;

use crate::load_cube::load_cube1;
use anyhow::{Error, Result};
use cgmath::{Matrix4, Point3, Quaternion, Rotation3, Vector3};
use futures::StreamExt;
use w as W;
use wgpu as w;
use wgpu::util::{BufferInitDescriptor, DeviceExt};
use wgpu::vertex_attr_array;
use wgpu::*;
use wgpu::{Adapter, Buffer, Instance};
use winit::window::Window;

fn main() {
    let mut scene_state = SceneState::default();
    let event_loop = winit::event_loop::EventLoop::new();
    let window = winit::window::Window::new(&event_loop).unwrap();
    use winit::event::WindowEvent;
    let mut context: WgpuContext = futures::executor::block_on(WgpuContext::new(&window));
    use winit::event::Event;
    use winit::event_loop::ControlFlow;
    use winit::event_loop::EventLoop;

    event_loop.run(move |event, _, control_flow| match event {
        Event::RedrawRequested(_) => {
            context.update();
            match context.render(scene_state.scene) {
                Ok(_) => {}
                Err(wgpu::SwapChainError::Lost) => {}
                Err(wgpu::SwapChainError::OutOfMemory) => *control_flow = ControlFlow::Exit,
                Err(e) => eprintln!("{:?}", e),
            }
        }
        Event::MainEventsCleared => {
            window.request_redraw();
        }
        Event::WindowEvent {
            event, window_id, ..
        } if window_id == window.id() => {
            if !context.input(&event) {
                match event {
                    WindowEvent::Resized(new_size) => context.resize(new_size),
                    WindowEvent::CloseRequested => {
                        *control_flow = winit::event_loop::ControlFlow::Exit
                    }
                    WindowEvent::KeyboardInput { input, .. } => match input {
                        KeyboardInput {
                            state: ElementState::Pressed,
                            virtual_keycode: Some(VirtualKeyCode::Escape),
                            ..
                        } => {
                            *control_flow = winit::event_loop::ControlFlow::Exit;
                        }
                        KeyboardInput {
                            state: ElementState::Pressed,
                            virtual_keycode: Some(VirtualKeyCode::Key1),
                            ..
                        } => {
                            scene_state.scene = Scenes::Terrain;
                        }

                        KeyboardInput {
                            state: ElementState::Pressed,
                            virtual_keycode: Some(VirtualKeyCode::Key2),
                            ..
                        } => {
                            scene_state.scene = Scenes::TerrainDebugLines;
                        }
                        _ => {}
                    },

                    _ => {}
                }
            }
        }

        _ => {}
    });
}
#[derive(Clone, Copy, Debug)]
pub enum Scenes {
    Terrain,
    TerrainDebugLines,
}
#[derive(Clone, Debug)]
pub struct SceneState {
    scene: Scenes,
}

impl std::default::Default for SceneState {
    fn default() -> Self {
        use Scenes::*;
        Self { scene: Terrain }
    }
}

pub struct WgpuContext {
    instance: wgpu::Instance,
    surface: wgpu::Surface,
    device: wgpu::Device,
    queue: wgpu::Queue,
    pub vertex_buffer: Buffer,
    pub swap_chain: SwapChain,
    pub render_pipeline: RenderPipeline,
    pub sc_desc: SwapChainDescriptor,
    pub index_buffer: Buffer,
    pub diffuse_bind_group: BindGroup,
    pub idx_buffer_len: u32,
    pub camera: Camera,
    pub uniform_buffer: Buffer,
    pub uniform_bind_group: BindGroup,
    pub camera_controller: CameraController,
    pub uniforms: Uniforms,
    pub depth_texture: crate::texture::Texture,
    pub render_pipeline_lines: RenderPipeline,
    pub instance_buffer: Buffer,
    pub instances: Vec<Instances>,
    pub instance_data: Vec<InstanceRaw>,
    pub cube_index_buffer: Buffer,
    pub cube_vertex_buffer: Buffer,
    pub cube_index_buffer_len: usize,
    pub cube_instances: Vec<InstanceRaw>,
    pub cube_instances_len: usize,
    pub cube_instance_buffer: Buffer,
    pub light_buffer: Buffer,
    pub light_buffer_data: Vec<Light>,
    pub light_bind_group: BindGroup,
    pub light_bind_group_layout: BindGroupLayout,
    pub material_bind_group: BindGroup,
    pub material_bind_group_layout: BindGroupLayout,
}

impl WgpuContext {
    fn input(&mut self, event: &WindowEvent) -> bool {
        self.camera_controller.process_events(event)
    }

    fn update(&mut self) {
        self.camera_controller.update_camera(&mut self.camera);
        self.uniforms.update_view_proj(&self.camera);
        //todo write buffer only when dirty
        self.queue
            .write_buffer(&self.uniform_buffer, 0, bytemuck::bytes_of(&self.uniforms));
    }

    fn resize(&mut self, new_size: winit::dpi::PhysicalSize<u32>) {
        self.sc_desc.width = new_size.width;
        self.sc_desc.height = new_size.height;
        self.swap_chain = self.device.create_swap_chain(&self.surface, &self.sc_desc);
    }

    pub async fn new(window: &Window) -> Self {
        let instance = Instance::new(wgpu::BackendBit::PRIMARY);
        let surface = unsafe { instance.create_surface(window) };
        let adapter: Adapter = instance
            .request_adapter(&W::RequestAdapterOptions {
                power_preference: W::PowerPreference::HighPerformance,
                compatible_surface: Some(&surface),
            })
            .await
            .unwrap();

        let (device, mut queue): (wgpu::Device, wgpu::Queue) = adapter
            .request_device(
                &wgpu::DeviceDescriptor {
                    label: Some("Device"),
                    features: wgpu::Features::empty() | wgpu::Features::NON_FILL_POLYGON_MODE,
                    limits: wgpu::Limits::default(),
                },
                Some(std::path::Path::new("../log.txt")),
            )
            .await
            .unwrap();

        use wgpu::util::DeviceExt;

        let (cube_verts, cube_indices, material) = load_cube1().unwrap();

        let cube_vertex_buffer = device.create_buffer_init(&BufferInitDescriptor {
            label: Some("Cube vertex"),
            contents: bytemuck::cast_slice(&cube_verts),
            usage: BufferUsage::VERTEX,
        });
        let cube_index_buffer = device.create_buffer_init(&BufferInitDescriptor {
            label: Some("Index buffer"),
            contents: bytemuck::cast_slice(&cube_indices),
            usage: BufferUsage::INDEX,
        });
        let cube_index_buffer_len = cube_indices.len();

        let mut cube_instances = vec![];

        let i = Instances {
            position: Vector3 {
                x: 0.0,
                y: 0.0,
                z: 0.0,
            },
            rotation: Quaternion {
                s: 1.0,
                v: Vector3 {
                    x: 0.0,
                    y: 0.0,
                    z: 0.0,
                },
            },
        };

        cube_instances.push(i.to_raw());
        let cube_instances_len = cube_instances.len();

        let cube_instance_buffer = device.create_buffer_init(&BufferInitDescriptor {
            label: Some("Cube instance buffer"),
            contents: bytemuck::cast_slice(&cube_instances),
            usage: BufferUsage::VERTEX,
        });

        use crate::terrain::terrain;
        let (verts, indices) = terrain(10, 10);
        let vertex_buffer = device.create_buffer_init(&BufferInitDescriptor {
            label: Some("Vertex buffer"),
            contents: bytemuck::cast_slice(&verts),
            usage: BufferUsage::VERTEX,
        });

        // let vertex_buffer: wgpu::Buffer =
        //     device.create_buffer_init(&wgpu::util::BufferInitDescriptor {
        //         label: Some("Vertex buffer"),
        //         contents: bytemuck::cast_slice(&VERTICES),
        //         usage: wgpu::BufferUsage::VERTEX,
        //     });

        let index_buffer: Buffer = device.create_buffer_init(&wgpu::util::BufferInitDescriptor {
            label: Some("Index buffer"),
            contents: bytemuck::cast_slice(&indices),
            usage: BufferUsage::INDEX,
        });

        let idx_buffer_len: u32 = indices.iter().count() as _;

        let size = window.inner_size();
        let sc_desc = wgpu::SwapChainDescriptor {
            usage: wgpu::TextureUsage::RENDER_ATTACHMENT,
            format: wgpu::TextureFormat::Bgra8UnormSrgb,
            width: size.width,
            height: size.height,
            present_mode: wgpu::PresentMode::Fifo,
        };

        let depth_texture =
            texture::Texture::create_depth_texture(&device, &sc_desc, "depth_texture");
        let diffuse_bytes = include_bytes!("rock_grey2vine.png");
        let diffuse_image = image::load_from_memory(diffuse_bytes).unwrap();
        let diffuse_rgba = diffuse_image.as_rgba8().unwrap();
        use image::{GenericImage, GenericImageView};

        let dimensions = diffuse_image.dimensions();
        let texture_size = Extent3d {
            width: dimensions.0,
            height: dimensions.1,
            depth: 1,
        };
        let diffuse_texture = device.create_texture(&wgpu::TextureDescriptor {
            label: Some("Happy tree"),
            size: texture_size.clone(),
            mip_level_count: 1,
            sample_count: 1,
            dimension: TextureDimension::D2,
            format: TextureFormat::Rgba8UnormSrgb,
            usage: TextureUsage::SAMPLED | TextureUsage::COPY_DST,
        });
        queue.write_texture(
            TextureCopyView {
                texture: &diffuse_texture,
                mip_level: 0,
                origin: Origin3d::ZERO,
            },
            diffuse_rgba as &[u8],
            TextureDataLayout {
                offset: 0,
                bytes_per_row: 4 * dimensions.0,
                rows_per_image: 4 * dimensions.1,
            },
            texture_size,
        );

        let diffuse_texture_view = diffuse_texture.create_view(&TextureViewDescriptor::default());
        let diffuse_sampler = device.create_sampler(&SamplerDescriptor {
            address_mode_u: AddressMode::MirrorRepeat,
            address_mode_v: AddressMode::MirrorRepeat,
            address_mode_w: AddressMode::ClampToEdge,
            mag_filter: FilterMode::Linear,
            min_filter: FilterMode::Nearest,
            mipmap_filter: FilterMode::Nearest,
            ..SamplerDescriptor::default()
        });

        let texture_bind_group_layout =
            device.create_bind_group_layout(&BindGroupLayoutDescriptor {
                label: Some("texture bind group layout"),
                entries: &[
                    BindGroupLayoutEntry {
                        binding: 0,
                        visibility: ShaderStage::FRAGMENT,
                        ty: BindingType::Texture {
                            multisampled: false,
                            view_dimension: TextureViewDimension::D2,
                            sample_type: wgpu::TextureSampleType::Float { filterable: false },
                        },
                        count: None,
                    },
                    BindGroupLayoutEntry {
                        binding: 1,
                        visibility: ShaderStage::FRAGMENT,
                        ty: BindingType::Sampler {
                            filtering: false,
                            comparison: false,
                        },
                        count: None,
                    },
                ],
            });

        let diffuse_bind_group = device.create_bind_group(&BindGroupDescriptor {
            label: Some("Diffuse Bind group 1"),
            layout: &texture_bind_group_layout,
            entries: &[
                BindGroupEntry {
                    binding: 0,
                    resource: BindingResource::TextureView(&diffuse_texture_view),
                },
                BindGroupEntry {
                    binding: 1,
                    resource: BindingResource::Sampler(&diffuse_sampler),
                },
            ],
        });
        let uniform_bind_group_layout =
            device.create_bind_group_layout(&wgpu::BindGroupLayoutDescriptor {
                entries: &[wgpu::BindGroupLayoutEntry {
                    binding: 0,
                    visibility: wgpu::ShaderStage::VERTEX | ShaderStage::FRAGMENT,
                    ty: wgpu::BindingType::Buffer {
                        ty: BufferBindingType::Uniform,
                        has_dynamic_offset: false,
                        min_binding_size: None,
                    },
                    count: None,
                }],
                label: Some("uniform_bind_group_layout"),
            });

        let material_bind_group_layout =
            device.create_bind_group_layout(&BindGroupLayoutDescriptor {
                label: Some("Material bind group"),
                entries: &[BindGroupLayoutEntry {
                    binding: 0,
                    visibility: ShaderStage::FRAGMENT,
                    ty: BindingType::Buffer {
                        ty: BufferBindingType::Uniform,
                        has_dynamic_offset: false,
                        min_binding_size: None,
                    },
                    count: None,
                }],
            });

        let material_buffer = device.create_buffer_init(&BufferInitDescriptor {
            label: Some("Material uniform buffer"),
            contents: bytemuck::cast_slice(&[material]),
            usage: BufferUsage::UNIFORM | BufferUsage::COPY_DST,
        });
        let material_bind_group = device.create_bind_group(&BindGroupDescriptor {
            label: Some(""),
            layout: &material_bind_group_layout,
            entries: &[BindGroupEntry {
                binding: 0,
                resource: BindingResource::Buffer {
                    buffer: &material_buffer,
                    offset: 0,
                    size: None,
                },
            }],
        });

        let vs_module =
            device.create_shader_module(&wgpu::include_spirv!("shaders/shader.vert.spv"));
        let fs_module =
            device.create_shader_module(&wgpu::include_spirv!("shaders/shader.frag.spv"));
        let fs_module_debug = device
            .create_shader_module(&wgpu::include_spirv!("shaders/shader_debuglines.frag.spv"));

        let (light_buffer_data, light_buffer, light_bind_group_layout, light_bind_group) =
            init_lights(&device);
        let render_pipeline_layout = device.create_pipeline_layout(&PipelineLayoutDescriptor {
            label: Some("Render pipeline laoyut"),
            bind_group_layouts: &[
                &texture_bind_group_layout,
                &uniform_bind_group_layout,
                &light_bind_group_layout,
                &material_bind_group_layout,
            ],
            push_constant_ranges: &[],
        });
        let render_pipeline_descriptor = RenderPipelineDescriptor {
            label: Some("Render pipeline desc"),
            layout: Some(&render_pipeline_layout),
            vertex: VertexState {
                module: &vs_module,
                entry_point: "main",
                buffers: &[Vertex::desc(), InstanceRaw::desc()],
            },
            primitive: PrimitiveState {
                topology: PrimitiveTopology::TriangleList,
                cull_mode: CullMode::None,
                polygon_mode: PolygonMode::Fill,
                front_face: FrontFace::Cw,
                strip_index_format: None,
            },
            depth_stencil: Some(DepthStencilState {
                format: TextureFormat::Depth32Float,
                depth_write_enabled: true,
                depth_compare: CompareFunction::Never,
                stencil: Default::default(),
                bias: Default::default(),
                clamp_depth: false
            }),
            multisample: MultisampleState {
                count: 1,
                mask: !0,
                alpha_to_coverage_enabled: true,
            },
            fragment: Some(FragmentState {
                module: &fs_module,
                entry_point: "main",
                targets: &[ColorTargetState {
                    format: TextureFormat::Bgra8UnormSrgb,
                    alpha_blend: BlendState::REPLACE,
                    color_blend: BlendState::REPLACE,
                    write_mask: ColorWrite::ALL,
                }],
            }),
        };
        let render_pipeline = device.create_render_pipeline(&render_pipeline_descriptor);

        let render_pipeline_descriptor_lines = RenderPipelineDescriptor {
            label: Some("Render pipeline lines desc"),
            layout: Some(&render_pipeline_layout),
            vertex: VertexState {
                module: &vs_module,
                entry_point: "main",
                buffers: &[Vertex::desc(), InstanceRaw::desc()],
            },
            primitive: PrimitiveState {
                topology: PrimitiveTopology::TriangleList,
                cull_mode: CullMode::None,
                polygon_mode: PolygonMode::Fill,
                front_face: FrontFace::Cw,
                strip_index_format: None,
            },
            depth_stencil: None,
            multisample: Default::default(),
            fragment: Some(FragmentState {
                module: &fs_module_debug,
                entry_point: "main",
                targets: &[ColorTargetState {
                    format: TextureFormat::Bgra8UnormSrgb,
                    alpha_blend: BlendState::REPLACE,
                    color_blend: BlendState::REPLACE,
                    write_mask: ColorWrite::ALL,
                }],
            }),
        };

        let render_pipeline_lines =
            device.create_render_pipeline(&render_pipeline_descriptor_lines);

        let swap_chain = device.create_swap_chain(&surface, &sc_desc);
        let camera = Camera {
            // position the camera one unit up and 2 units back
            // +z is out of the screen
            eye: Point3::new(0.0, 1.0, 2.0),
            // have it look at the origin
            target: Point3::new(0.0, 0.0, 0.0),
            // which way is "up"
            up: cgmath::Vector3::unit_y(),
            aspect: sc_desc.width.clone() as f32 / sc_desc.height.clone() as f32,
            fovy: 45.0,
            znear: 0.1,
            zfar: 2000.0,
        };
        let mut uniforms = Uniforms::new();
        uniforms.update_view_proj(&camera);

        let uniform_buffer = device.create_buffer_init(&wgpu::util::BufferInitDescriptor {
            label: Some("Uniform Buffer"),
            contents: bytemuck::bytes_of(&uniforms),
            usage: wgpu::BufferUsage::UNIFORM | wgpu::BufferUsage::COPY_DST,
        });

        let uniform_bind_group = device.create_bind_group(&wgpu::BindGroupDescriptor {
            layout: &uniform_bind_group_layout,
            entries: &[wgpu::BindGroupEntry {
                binding: 0,
                resource: wgpu::BindingResource::Buffer {
                    /* fields */ buffer: &uniform_buffer,
                    offset: 0,
                    size: None,
                },
            }],
            label: Some("uniform_bind_group"),
        });

        let camera_controller = CameraController::new(0.2);
        use cgmath::InnerSpace;
        let mut instances = vec![];

        let n = 1;
        for x in 0..n {
            for y in 0..n {
                let x = -n as f32 / 2.0 * 100.0 + x as f32 * 100.0;
                let z = -n as f32 / 2.0 * 100.0 + y as f32 * 100.0;
                instances.push(Instances {
                    position: Vector3 { x, y: 0.0, z },
                    rotation: Quaternion {
                        s: 1.0,
                        v: Vector3 {
                            x: 0.0,
                            y: 0.0,
                            z: 0.0,
                        },
                    },
                });
            }
        }

        let instance_data = instances.iter().map(Instances::to_raw).collect::<Vec<_>>();
        let instance_buffer = device.create_buffer_init(&wgpu::util::BufferInitDescriptor {
            label: Some("Instance Buffer"),
            contents: bytemuck::cast_slice(&instance_data),
            usage: wgpu::BufferUsage::VERTEX,
        });

        WgpuContext {
            instance,
            surface,
            device,
            queue,
            vertex_buffer,
            index_buffer,
            sc_desc,
            swap_chain,
            render_pipeline,
            diffuse_bind_group,
            idx_buffer_len,
            camera,
            uniform_buffer,
            uniform_bind_group,
            camera_controller,
            uniforms,
            depth_texture,
            render_pipeline_lines,
            instances,
            instance_buffer,
            instance_data,
            cube_vertex_buffer,
            cube_index_buffer,
            cube_index_buffer_len,
            cube_instances,
            cube_instances_len,
            cube_instance_buffer,
            light_buffer,
            light_buffer_data,
            light_bind_group_layout,
            light_bind_group,
            material_bind_group,
            material_bind_group_layout,
        }
    }

    fn render(&self, scene: Scenes) -> Result<(), wgpu::SwapChainError> {
        let frame = self.swap_chain.get_current_frame()?.output;

        let mut encoder = self
            .device
            .create_command_encoder(&CommandEncoderDescriptor {
                label: Some("Command encoder 1"),
            });

        {
            let mut render_pass = encoder.begin_render_pass(&RenderPassDescriptor {
                label: Some("Render pass 1"),
                color_attachments: &[RenderPassColorAttachmentDescriptor {
                    attachment: &frame.view,
                    resolve_target: None,
                    ops: Operations {
                        load: LoadOp::Clear(Color::BLUE),
                        store: true,
                    },
                }],
                depth_stencil_attachment: Some(RenderPassDepthStencilAttachmentDescriptor {
                    attachment: &self.depth_texture.view,
                    depth_ops: Some(Operations {
                        load: LoadOp::Clear(1.0),
                        store: true,
                    }),
                    stencil_ops: None,
                }),
            });

            use Scenes::*;
            match scene {
                Terrain => {
                    let mut render_pass = self.draw_pipeline(render_pass);
                    let mut render_pass = self.draw_box(render_pass);
                    render_pass
                }

                TerrainDebugLines => {
                    let mut render_pass = self.draw_pipeline_debug(render_pass);
                    self.draw_box_debug(render_pass)
                }
            };
        }
        self.queue.submit(std::iter::once(encoder.finish()));
        Ok(())
    }

    fn draw_pipeline_debug<'a, 'b: 'a>(
        &'b self,
        mut render_pass: RenderPass<'a>,
    ) -> RenderPass<'a> {
        render_pass.set_pipeline(&self.render_pipeline_lines);
        render_pass.set_bind_group(0, &self.diffuse_bind_group, &[]);
        render_pass.set_bind_group(1, &self.uniform_bind_group, &[]);
        render_pass.set_bind_group(2, &self.light_bind_group, &[]);
        render_pass.set_bind_group(3, &self.material_bind_group, &[]);

        render_pass.set_vertex_buffer(0, self.vertex_buffer.slice(..));
        render_pass.set_vertex_buffer(1, self.instance_buffer.slice(..));
        render_pass.set_index_buffer(self.index_buffer.slice(..), IndexFormat::Uint32);
        render_pass.draw_indexed(0..self.idx_buffer_len, 0, 0..self.instances.len() as _);
        render_pass
    }

    fn draw_box<'a, 'b: 'a>(&'b self, mut render_pass: RenderPass<'a>) -> RenderPass<'a> {
        render_pass.set_pipeline(&self.render_pipeline);
        render_pass.set_bind_group(0, &self.diffuse_bind_group, &[]);
        render_pass.set_bind_group(1, &self.uniform_bind_group, &[]);
        render_pass.set_bind_group(2, &self.light_bind_group, &[]);
        render_pass.set_bind_group(3, &self.material_bind_group, &[]);
        render_pass.set_vertex_buffer(0, self.cube_vertex_buffer.slice(..));
        render_pass.set_vertex_buffer(1, self.cube_instance_buffer.slice(..));
        render_pass.set_index_buffer(self.cube_index_buffer.slice(..), IndexFormat::Uint32);
        render_pass.draw_indexed(
            0..self.cube_index_buffer_len as _,
            0,
            0..self.cube_instances_len as _,
        );
        render_pass
    }

    fn draw_box_debug<'a, 'b: 'a>(&'b self, mut render_pass: RenderPass<'a>) -> RenderPass<'a> {
        render_pass.set_pipeline(&self.render_pipeline_lines);
        render_pass.set_bind_group(0, &self.diffuse_bind_group, &[]);
        render_pass.set_bind_group(1, &self.uniform_bind_group, &[]);
        render_pass.set_bind_group(2, &self.light_bind_group, &[]);
        render_pass.set_bind_group(3, &self.material_bind_group, &[]);
        render_pass.set_vertex_buffer(0, self.cube_vertex_buffer.slice(..));
        render_pass.set_vertex_buffer(1, self.cube_instance_buffer.slice(..));
        render_pass.set_index_buffer(self.cube_index_buffer.slice(..), IndexFormat::Uint32);
        render_pass.draw_indexed(
            0..self.cube_index_buffer_len as _,
            0,
            0..self.cube_instances_len as _,
        );
        render_pass
    }

    fn draw_pipeline<'a, 'b: 'a>(&'b self, mut render_pass: RenderPass<'a>) -> RenderPass<'a> {
        render_pass.set_pipeline(&self.render_pipeline);
        render_pass.set_bind_group(0, &self.diffuse_bind_group, &[]);
        render_pass.set_bind_group(1, &self.uniform_bind_group, &[]);
        render_pass.set_bind_group(2, &self.light_bind_group, &[]);
        render_pass.set_bind_group(3, &self.material_bind_group, &[]);
        render_pass.set_vertex_buffer(0, self.vertex_buffer.slice(..));
        render_pass.set_vertex_buffer(1, self.instance_buffer.slice(..));
        render_pass.set_index_buffer(self.index_buffer.slice(..), IndexFormat::Uint32);
        render_pass.draw_indexed(0..self.idx_buffer_len, 0, 0..self.instances.len() as _);

        render_pass
    }
}

const VERTICES: &[Vertex] = &[
    Vertex {
        position: [-0.0868241, 0.49240386, 0.0, 1.0],
        normal: [0.0; 4],
        tex_coords: [0.4131759, 0.00759614],
        tangent: [0.0; 4],
    }, // Aa
    Vertex {
        position: [-0.49513406, 0.06958647, 0.0, 1.0],
        tex_coords: [0.0048659444, 0.43041354],
        normal: [0.0; 4],
        tangent: [0.0; 4],
    }, // B
    Vertex {
        position: [-0.21918549, -0.44939706, 0.0, 1.0],
        tex_coords: [0.28081453, 0.949397057],
        normal: [0.0; 4],
        tangent: [0.0; 4],
    }, // C
];
#[derive(Debug, Clone, Copy, Default)]
pub struct Vertex {
    position: [f32; 4],
    normal: [f32; 4],
    tangent: [f32; 4],
    tex_coords: [f32; 2],
}

#[repr(C)]
#[derive(Clone, Debug, Copy, bytemuck::Pod, bytemuck::Zeroable, Default)]
pub struct Light {
    position: [f32; 4],
    // ambient: [f32; 4],
    // diffuse: [f32; 4],
    // specular: [f32; 4],
}

unsafe impl bytemuck::Pod for Vertex {}
unsafe impl bytemuck::Zeroable for Vertex {}

impl Vertex {
    fn desc<'a>() -> wgpu::VertexBufferLayout<'a> {
        VertexBufferLayout {
            array_stride: std::mem::size_of::<Self>() as BufferAddress,
            step_mode: InputStepMode::Vertex,
            attributes: &[
                VertexAttribute{
                    //pos
                    offset: 0,
                    format: VertexFormat::Float4,
                    shader_location: 0,
                },
                VertexAttribute{
                    // normal
                    offset: 16,
                    format: VertexFormat::Float4,
                    shader_location: 1,
                },
                VertexAttribute{
                    // tangent
                    offset: 32,
                    format: VertexFormat::Float4,
                    shader_location: 2,
                },
                VertexAttribute{
                    //texcoords
                    offset: 48,
                    format: VertexFormat::Float2,
                    shader_location: 3,
                },
            ]
        }
    }
    fn from_easy_gltf(v: &easy_gltf::model::Vertex) -> Self {
        let v = v.clone();
        Self {
            position: [v.position.x, v.position.y, v.position.z, 1.0],
            normal: [v.normal.x, v.normal.y, v.normal.z, 1.0],
            tangent: [v.tangent.x, v.tangent.y, v.tangent.z, 1.0],
            tex_coords: [v.tex_coords.x, v.tex_coords.y],
        }
    }
}

pub mod load_cube {
    use crate::{PbrMaterial, Vertex};
    use anyhow::{Error, Result};

    pub(crate) fn load_cube() -> Result<Vec<()>> {
        Ok(vec![])
    }

    pub(crate) fn load_cube1() -> Result<(Vec<Vertex>, Vec<u32>, PbrMaterial)> {
        use easy_gltf::*;

        let scenes: Vec<Scene> = load(std::path::Path::new("untitled.glb")).unwrap();

        let model: &Model = &scenes[0].models[0];
        let verts = model
            .vertices()
            .into_iter()
            .map(Vertex::from_easy_gltf)
            .collect::<Vec<_>>();
        let indices = model
            .indices()
            .unwrap()
            .into_iter()
            .map(|x| *x as u32)
            .collect::<Vec<u32>>();

        let material = model.material();
        let pbr: &easy_gltf::model::PbrMaterial = &material.pbr;

        let pbr_material = PbrMaterial {
            base_colour_factor: pbr.base_color_factor.into(),
            metallic_factor: pbr.metallic_factor,
            roughness_factor: pbr.roughness_factor,
        };
        Ok((verts, indices, pbr_material))
    }
}
#[repr(C)]
#[derive(Clone, Copy, Debug, bytemuck::Zeroable, bytemuck::Pod)]
pub struct PbrMaterial {
    pub base_colour_factor: [f32; 4],
    pub metallic_factor: f32,
    pub roughness_factor: f32,
}

#[rustfmt::skip]
pub const OPENGL_TO_WGPU_MATRIX: cgmath::Matrix4<f32> = cgmath::Matrix4::new(
    1.0, 0.0, 0.0, 0.0,
    0.0, 1.0, 0.0, 0.0,
    0.0, 0.0, 0.5, 0.0,
    0.0, 0.0, 0.5, 1.0,
);

pub struct Camera {
    eye: cgmath::Point3<f32>,
    target: cgmath::Point3<f32>,
    up: cgmath::Vector3<f32>,
    aspect: f32,
    fovy: f32,
    znear: f32,
    zfar: f32,
}
impl Camera {
    fn build_projection_matrix(&self) -> cgmath::Matrix4<f32> {
        // 1.
        let view = cgmath::Matrix4::look_at(self.eye.clone(), self.target.clone(), self.up.clone());
        // 2.
        let proj = cgmath::perspective(
            cgmath::Deg(self.fovy.clone()),
            self.aspect.clone(),
            self.znear.clone(),
            self.zfar.clone(),
        );

        // 3.
        return OPENGL_TO_WGPU_MATRIX * proj * view;
    }
}

#[repr(C)]
#[derive(Copy, Clone, bytemuck::Pod, bytemuck::Zeroable, Default)]
pub struct Uniforms {
    view_proj: [f32; 16],
    camera_position: [f32; 3],
    _padding: [f32; 16 - 3],
}

impl Uniforms {
    fn new() -> Self {
        use cgmath::SquareMatrix;
        Self {
            view_proj: unsafe { std::mem::transmute_copy(&cgmath::Matrix4::<f32>::identity()) },
            camera_position: [0.0; 3].into(),
            ..std::default::Default::default()
        }
    }

    fn update_view_proj(&mut self, camera: &Camera) {
        self.view_proj = unsafe {
            std::mem::transmute_copy(
                &(OPENGL_TO_WGPU_MATRIX * camera.build_view_projection_matrix()),
            )
        };
        self.camera_position = camera.eye.into();
    }
}

impl Camera {
    fn build_view_projection_matrix(&self) -> cgmath::Matrix4<f32> {
        let view = cgmath::Matrix4::look_at(self.eye, self.target, self.up);
        let proj = cgmath::perspective(cgmath::Deg(self.fovy), self.aspect, self.znear, self.zfar);
        proj * view
    }
}
pub struct CameraController {
    speed: f32,
    is_up_pressed: bool,
    is_down_pressed: bool,
    is_forward_pressed: bool,
    is_backward_pressed: bool,
    is_left_pressed: bool,
    is_right_pressed: bool,
}
use image::RgbaImage;
use std::path::Path;
use wgpu_subscriber::initialize_default_subscriber;
use winit::event::{ElementState, KeyboardInput, VirtualKeyCode, WindowEvent};

impl CameraController {
    fn new(speed: f32) -> Self {
        Self {
            speed,
            is_up_pressed: false,
            is_down_pressed: false,
            is_forward_pressed: false,
            is_backward_pressed: false,
            is_left_pressed: false,
            is_right_pressed: false,
        }
    }

    fn process_events(&mut self, event: &WindowEvent) -> bool {
        match event {
            WindowEvent::KeyboardInput {
                input:
                    KeyboardInput {
                        state,
                        virtual_keycode: Some(keycode),
                        ..
                    },
                ..
            } => {
                let is_pressed = *state == ElementState::Pressed;
                match keycode {
                    VirtualKeyCode::Space => {
                        self.is_up_pressed = is_pressed;
                        true
                    }
                    VirtualKeyCode::LShift => {
                        self.is_down_pressed = is_pressed;
                        true
                    }
                    VirtualKeyCode::W | VirtualKeyCode::Up => {
                        self.is_forward_pressed = is_pressed;
                        true
                    }
                    VirtualKeyCode::A | VirtualKeyCode::Left => {
                        self.is_left_pressed = is_pressed;
                        true
                    }
                    VirtualKeyCode::S | VirtualKeyCode::Down => {
                        self.is_backward_pressed = is_pressed;
                        true
                    }
                    VirtualKeyCode::D | VirtualKeyCode::Right => {
                        self.is_right_pressed = is_pressed;
                        true
                    }
                    _ => false,
                }
            }
            _ => false,
        }
    }

    fn update_camera(&self, camera: &mut Camera) {
        use cgmath::InnerSpace;
        let forward = camera.target - camera.eye;
        let forward_norm = forward.normalize();
        let forward_mag = forward.magnitude();

        // Prevents glitching when camera gets too close to the
        // center of the scene.
        if self.is_forward_pressed && forward_mag > self.speed {
            camera.eye += forward_norm * self.speed;
        }
        if self.is_backward_pressed {
            camera.eye -= forward_norm * self.speed;
        }

        let right = forward_norm.cross(camera.up);

        // Redo radius calc in case the up/ down is pressed.
        let forward = camera.target - camera.eye;
        let forward_mag = forward.magnitude();

        if self.is_right_pressed {
            // Rescale the distance between the target and eye so
            // that it doesn't change. The eye therefore still
            // lies on the circle made by the target and eye.
            camera.eye = camera.target - (forward + right * self.speed).normalize() * forward_mag;
        }
        if self.is_left_pressed {
            camera.eye = camera.target - (forward - right * self.speed).normalize() * forward_mag;
        }
    }
}

pub struct Instances {
    position: cgmath::Vector3<f32>,
    rotation: cgmath::Quaternion<f32>,
}

#[repr(C)]
#[derive(Copy, Clone, bytemuck::Pod, bytemuck::Zeroable)]
pub struct InstanceRaw {
    model: [[f32; 4]; 4],
}

impl InstanceRaw {
    fn desc<'a>() -> VertexBufferLayout<'a> {
        use std::mem;
        VertexBufferLayout {
            array_stride: mem::size_of::<InstanceRaw>() as BufferAddress,
            step_mode: InputStepMode::Instance,
            attributes: &[
                VertexAttribute{
                    offset: 0,
                    shader_location: 4,
                    format: VertexFormat::Float4,
                },
                VertexAttribute{
                    offset: 1 * mem::size_of::<[f32; 4]>() as BufferAddress,
                    shader_location: 5,
                    format: VertexFormat::Float4,
                },
                VertexAttribute{
                    offset: 2 * mem::size_of::<[f32; 4]>() as BufferAddress,
                    shader_location: 6,
                    format: VertexFormat::Float4,
                },
                VertexAttribute{
                    offset: 3 * mem::size_of::<[f32; 4]>() as BufferAddress,
                    shader_location: 7,
                    format: VertexFormat::Float4,
                },
            ],
        }
    }
}

impl Instances {
    fn to_raw(&self) -> InstanceRaw {
        InstanceRaw {
            model: (cgmath::Matrix4::from_translation(self.position)
                * cgmath::Matrix4::from(self.rotation))
            .into(),
        }
    }
}

struct State {
    instances: Vec<Instance>,
    instance_buffer: wgpu::Buffer,
}

const NUM_INSTANCES_PER_ROW: u32 = 3;
const NUM_INSTANCES: u32 = NUM_INSTANCES_PER_ROW * NUM_INSTANCES_PER_ROW;
const INSTANCE_DISPLACEMENT: cgmath::Vector3<f32> = cgmath::Vector3::new(
    NUM_INSTANCES_PER_ROW as f32 * 2.0,
    0.0,
    NUM_INSTANCES_PER_ROW as f32 * 2.0,
);

fn init_lights(device: &Device) -> (Vec<Light>, W::Buffer, W::BindGroupLayout, W::BindGroup) {
    let light_buffer_data = vec![
        Light {
            position: [3.0, 15.0, 1.0, 1.0],
            ..Default::default()
        },
        Light {
            position: [3.0, 15.0, 56.0, 1.0],
            ..Default::default()
        },
    ];
    let light_buffer = device.create_buffer_init(&wgpu::util::BufferInitDescriptor {
        label: Some("Light buffer"),
        contents: bytemuck::cast_slice(&light_buffer_data),
        usage: wgpu::BufferUsage::UNIFORM,
    });
    use core::num::NonZeroU32;
    let light_bind_group_layout = device.create_bind_group_layout(&BindGroupLayoutDescriptor {
        label: Some("Lights bind layout"),
        entries: &[BindGroupLayoutEntry {
            binding: 0,
            visibility: ShaderStage::VERTEX | ShaderStage::FRAGMENT,
            ty: BindingType::Buffer {
                ty: BufferBindingType::Uniform,
                has_dynamic_offset: false,
                min_binding_size: None,
            },
            count: None,
        }],
    });

    let light_bind_group = device.create_bind_group(&BindGroupDescriptor {
        label: Some("Light bind group"),
        layout: &light_bind_group_layout,
        entries: &[BindGroupEntry {
            binding: 0,
            resource: BindingResource::Buffer {
                buffer: &light_buffer,
                offset: 0,
                 size: {
                     let d = std::mem::size_of_val(&light_buffer_data) as u64;
                     W::BufferSize::new(d)
                 },
               
            },
        }],
    });
    (
        light_buffer_data,
        light_buffer,
        light_bind_group_layout,
        light_bind_group,
    )
}
